package Negocio;

public interface OperacionMatriz {
    
    public int[] getVectorDecimal() throws Exception;
    
    public boolean isDispersa() throws Exception;
    
}
