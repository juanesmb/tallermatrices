/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author juane
 */
public class MatrizBinaria implements OperacionMatriz{

    private boolean m[][];

    public MatrizBinaria() {
    }
    
    public MatrizBinaria(String datos) throws Exception{
        String [] x = datos.split(";");
        this.m = new boolean [x.length][];
        for (int i = 0; i < x.length; i++) {
            String [] p = x[i].split(",");
            this.m[i] = new boolean [p.length];
            for(int j = 0;j<m[i].length;j++)
            {
                if(p[j].equals("1"))
                    m[i][j] = true;
                if(p[j].equals("0"))
                    m[i][j] = false;
                else
                    throw new Exception ("tiene valores diferentes a 0 y 1");
                
            }
        }
    }
    
    @Override
    public int[] getVectorDecimal() throws Exception {
        if(this.m == null)
            throw new Exception ("la matriz no tiene elementos");
        int [] v = new int [m.length];
        int c = 0;
        for (int i = 0; i < m.length; i++) {
            for (int j = m[i].length-1; j >= 0; j--) {
                if(m[i][j] == true)
                    v[i] = (int) Math.pow(2,c);
                else
                    v[i] = 0;
                c++;
            }
        }
        return v;
    }

    @Override
    public boolean isDispersa() throws Exception {
        
        if(this.m == null)
            throw new Exception ("la matriz no tiene elementos");
        
        for (int i = 0; i < m.length; i++) {
            if(m[0].length != m[i].length)
                return true;
        }
        return false;
    }
    
    public MatrizBinaria getSumar(MatrizBinaria m2) throws Exception
    {
        if(!this.sonIguales(m2.getM()))
            throw new Exception ("las matrices no tienen la misma dimensión");
        int [][] matriz = new int [m.length][];
        for(int i = 0;i<m.length;i++)
        {
            matriz [i] = new int [m[i].length];
            for (int j = 0; j < m[i].length; j++) {
                if(m[i][j] == false && m2.getM()[i][j]==false)
                    matriz[i][j] = 0;
                if((m[i][j] == true && m2.getM()[i][j]==false) || (m[i][j] == false && m2.getM()[i][j]==true))  
                    matriz[i][j] = 1;
                if((m[i][j] == true && m2.getM()[i][j]==true) || (m[i][j] == true && m2.getM()[i][j]==true))  
                    matriz[i][j] = 2;
            }
        }
        String dato = aDatoBruto(matriz);
        MatrizBinaria b = new MatrizBinaria(dato);
        return b;
        
    }

    private String aDatoBruto(int[][] matriz) {
        String msg = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if(j < matriz[i].length-1)
                    msg = msg + matriz [i][j] + ",";
                else
                    msg = msg + matriz [i][j];
            }
            msg = msg + ";";
        }
        return msg;
    }
    
    private boolean sonIguales (boolean[][]m2)
    {
        if(this.m.length != m2.length)
            return false;
        for (int i = 0; i < m.length; i++) {
            if(this.m[i].length != m2[i].length)
                return false;
        }
        return true;
    }

    public boolean[][] getM() {
        return m;
    }

    @Override
    public String toString() {
        String msg = "";
        
        for (int i = 0; i < this.m.length; i++) {
            for (int j = 0; j < this.m[i].length; j++) {
                msg = msg + this.m + ",";
            }
            msg = msg + this.m + "\n";
        }
        return msg;
    }
    
    
    
}
