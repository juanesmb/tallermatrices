package Vista;
import Negocio.MatrizBinaria;
import javax.swing.JOptionPane;


public class Test {
    public static void main(String[] args) {
        String dato = JOptionPane.showInputDialog("Escribe la matriz");
        try
        {
            MatrizBinaria m = new MatrizBinaria(dato);
            System.out.println("Mi matriz es: " + m.toString());
            OperacionMatriz myMatriz = (OperacionMatriz)m;
            System.out.println("myMatriz es dispersa: " + m.isDispersa());
            int vectorDecimal[] = myMatriz.getVectorDecimal();
            for (int n:vectorDecimal) {
                System.out.println(n);
                String dato2 = JOptionPane.showInputDialog("Escribe la matriz 2");
                MatrizBinaria m2 = new MatrizBinaria (dato2);
                MatrizBinaria m3 = m.getSumar(m2);
                System.out.println("Mi matriz m3 es: " + m3.toString());
            } 
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
    }
    
}
